#include <vtkPointSource.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPointLocator.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;
 
vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}
 
double getEuclideanDistanceIn3D(double* p1, double* p2) 
{
	double s;
	s = ((p1[0] - p2[0])*(p1[0] - p2[0])) + ((p1[1] - p2[1])*(p1[1] - p2[1])) + ((p1[2] - p2[2])*(p1[2] - p2[2])); 
	return sqrt(s);
}
 
int main(int argc, char **argv)
{
  if (argc < 5)
  {
     cerr << "\nERROR: Not enough parameters found\nUsage: \nParam1 = data file containing xyz and scalar"
     "\nParam3 = diameter over which to spread data\nParam2 = vtk surface\nParam4 = Output vtk file"<< endl; 
     exit(1);
  }
  
  char* fn1 = argv[1];
  ifstream fin(fn1);
  string lif;
  double threshold_distance=5; 
  
  cout << "Reading file : " << fn1 << endl;
  
  if (argc > 4) {
    cout << "Threshold distance :" << argv[2];
    threshold_distance = atof(argv[2]); 
  } 
  else 
  {
    cout << "No threshold specified, using default value = " << threshold_distance << endl;
  }
  
  vector<string> xyz_s;
  char delim = '\t';
  
  vector<vector<double> > xyz; 
  vector<double> scalar; 
 
  /*
  * Read the file line by line 
  */ 
  while(getline(fin, lif)) 
	{
  
      split(lif, delim, xyz_s); 
       
      vector<double> xyz_temp; 
      xyz_temp.push_back(stod(xyz_s[0]));
      xyz_temp.push_back(stod(xyz_s[1]));
      xyz_temp.push_back(stod(xyz_s[2]));
      //cout << xyz_s[3] << endl;
      scalar.push_back(stod(xyz_s[3]));
      
      xyz.push_back(xyz_temp);  
      
      xyz_s.clear();  
     
  }
  fin.close();
  
  vtkSmartPointer<vtkPolyData> surface =vtkSmartPointer<vtkPolyData>::New();  
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New(); 
  reader->SetFileName(argv[3]); 
  reader->Update();
  
  surface = reader->GetOutput();
 
  vtkSmartPointer<vtkPointLocator > point_locator = vtkSmartPointer<vtkPointLocator >::New(); 
	point_locator->SetDataSet(surface); 
	point_locator->AutomaticOn(); 
	point_locator->BuildLocator(); 
  
  double* surface_xyz = new double[3]; 
  double* data_xyz = new double[3];
  
  vector<vector<vtkIdType> > point_ids; 
  
  for (int i=0;i<xyz.size();i++)
  {
    
     data_xyz[0] = xyz[i][0]; data_xyz[1] = xyz[i][1]; data_xyz[2] = xyz[i][2];
     vector<vtkIdType> point_id; 
        
     for (int k=0;k<surface->GetNumberOfPoints();k++)
     {
        surface->GetPoint(k, surface_xyz);
        double distance = getEuclideanDistanceIn3D(data_xyz, surface_xyz);
        
        if (distance < threshold_distance && distance >= 0)
        {
            point_id.push_back(k);
        }
     }
     
     point_ids.push_back(point_id);
    
  }
  
  
  vtkSmartPointer<vtkFloatArray> scalars = vtkSmartPointer<vtkFloatArray>::New();
  
   for (int i=0;i<xyz.size();i++)
   {
      double value = scalar[i];
      
      for (int k=0;k<point_ids[i].size();k++)
      {
         scalars->InsertTuple1(point_ids[i][k], value); 
      } 
   }
   
   surface->GetPointData()->SetScalars(scalars);
 
  
  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New(); 
  writer->SetFileName(argv[4]); 
  writer->SetInputData(surface); 
  writer->Update();
  
  
}