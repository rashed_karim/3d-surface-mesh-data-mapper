# Introduction 
This program is capable of reading sparse data from a text file and plotting it onto a VTK surface. 

It takes an input file that lists the sparse data as a set of points with their scalar values. See below. 

It spreads the scalar value within a certain radius of each point. Ideally, the points input should lie on or near the surface. 


## Usage 


```
#!python

Usage: 
Param1 = data file containing xyz and scalar
Param3 = radius over which to spread data
Param2 = vtk surface
Param4 = Output vtk file

```


## Sample input data ##

A text file containing lines that are:  X Y Z scalar_value 


```
#!python


122	133	-94	103.1542553
136	225	-201	166.1861702
181	253	-130	52.62234043
98	154	-119	130.8138298
100	170	-116	166.9840426
111	140	-138	171.7712766
160	165	-158	100.7606383
99	137	-136	171.7712766
114	158	-90	61.39893617
173	211	-139	70.97340426
128	161	-128	85.60106383
121	123	-108	77.62234043
108	145	-100	132.9414894
165	209	-124	-4.558510638
99	168	-151	165.9202128

```